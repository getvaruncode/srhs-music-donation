var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('express-jwt');

var authenticate = jwt({
  secret: new Buffer(process.env.AUTH0_CLIENT_SECRET, 'base64'),
  audience: process.env.AUTH0_CLIENT_ID
});

var MONGO_ENV = require('./lib/mongoEnv');

var mongoPath = MONGO_ENV.MONGO_URL;
var saveDonation = require('./lib/saveDonation');
var Donation = require('./lib/getDonations');

// The Stripe library being called here
var localCharge = require('./lib/charge');

console.log("MongoDB configured to use: " + mongoPath);

// parse application/json
app.use(bodyParser.json());

app.get('/helloworld', function (req, res) {
  res.send('Hello World!');
});

app.get('/auth0-variables.js', function (req, res) {
  var authObject = {
    client: process.env.AUTH0_CLIENT_ID,
    domain: process.env.AUTH0_DOMAIN
  };
  res.send("var AUTH0 = " + JSON.stringify(authObject));
});

app.use('/secured', authenticate);
app.use('/secured/ping', function (req, res) {
    res.json("You got through!");
});

app.use('/secured/getDonations', function (req, res) {

  console.log("Authorization header is " + JSON.stringify(req.headers.authorization));

  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  
  Donation.getDonations(completeDonationModel, startDate, endDate, function(err, result) {
    if (err) res.error({});
    res.json(result);
  });
});

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    console.error('Invalid credentials for request ' + JSON.stringify(req.headers.authorization) + ' ' + JSON.stringify(err));
    var returnObj = {"name": err.name, "message": err.message};
    res.status(401).send(returnObj);
  }
});

app.post('/donate', function (req, res) {
  //res.send('Hello World!');
  var stripeToken = req.body.stripeToken;
  var donation = req.body.donationDetails;
  saveDonation.saveWOPayment(req.body, donationWOPaymentModel, function(e, preCapture){
    if (e) { 
      console.log("Error in inserting preCapture " + JSON.stringify(e));
      res.send(e);
    } else {
      localCharge.charge((donation.amount*100), donation.donor.email, stripeToken.id, function (e,d) {
        if (e) {
          console.log("Charge failed " + JSON.stringify(e, null, 4));
          res.send(e);
        } else {
          d.preCaptureID = String(preCapture._id);
          console.log("Charge accepted " + JSON.stringify(d, null, 4));
          saveDonation.saveWithPayment(req.body, completeDonationModel, d, preCapture._id, function(e,data){
            if (e) {
              console.log("Could not write payment to DB: reg: " + req.body);
              console.log("Could not write payment to DB: pmt: " + d);
              console.log("Could not write payment to DB: error: " + e);
              res.send(d);
            }  else {
              d.regID = String(data._id);
              console.log("Wrote Registration " + data);
              res.send(d);
            }; 
            //Send Email Confirmation
            donation.regID = d.regID;
            emailObj.sendMail(donation, function(err, json) {
              if (!err) {
                console.log("Confirmation Email sent to " + donation.donor.email);
              } else {
                console.log("Email confirmation failed for " + donation.donor.name + "\n" + err);
              }
            }); // End of Email.sendEmail
        }); // End of saveWithPayment
      }; 
    });  // End of localCharge
  };
  }); // End of Save WO Payment
console.log("Donation is " + JSON.stringify(donation));
}); // End of app.post


app.use('/', express.static('public'));

var portNum = process.env.PORT || 8000;

mongoose.connection.on("connected", function(ref) {
  console.log("Connected to " + mongoPath + " DB! at " + new Date().toLocaleString());
  ip = process.env.ip;
  var server = app.listen(portNum, ip, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('SRHS Music Donation Application listening at http://%s:%s\n', host, port);
  });
});


try {
  options = {
        user: MONGO_ENV.MONGO_USER, 
        pass: MONGO_ENV.MONGO_PWD,
        server:  { autoreconnect: true, socketOptions:  { keepAlive: 1 }},
        replset: { socketOptions: { keepAlive: 1 }}
  };
  mongoose.connect(mongoPath, options);
  console.log("Trying to connect to DB " + mongoPath);
} catch (err) {
  console.log("Sever initialization failed " , err.message);
  process.exit(0);
}

/* ------------------------------ All Mongoose Connection functions -------------------- */

// If the connection throws an error
mongoose.connection.on("error", function(err) {
  console.error('Failed to connect to DB ' + mongoPath + ' on startup ', err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection to DB :' + mongoPath + ' disconnected at ' + new Date().toLocaleString());
});

// Exit Gracefully and close the connection
var gracefulExit = function() {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection with DB :' + mongoPath + ' is disconnected through app termination');
    process.exit(0);
  });
}

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);

var completeDonation = new mongoose.Schema({
      name: String,
      email: String,
      phone: String,
      donationGoesTo: String,
      employerMatch: Boolean,
      employer: String,
      subscribe: {
        general: Boolean,
        boosters: Boolean,
        band: Boolean,
        orch: Boolean,
        march: Boolean,
        guard: Boolean,
        jazz: Boolean,
        perc: Boolean,
      },
      payment: {
        tokenId: String,
        authkey: mongoose.Schema.Types.ObjectId,
        chargeId: String,
        amount: Number,
        cardLastFour: Number,
        brand: String
      },
      inserted: {type: Date, default: Date.now}
    });

var donationWOPayment = new mongoose.Schema({
      name: String,
      email: String,
      phone: String,
      donationGoesTo: String,
      employerMatch: Boolean,
      employer: String,
      subscribe: {
        general: Boolean,
        boosters: Boolean,
        band: Boolean,
        orch: Boolean,
        march: Boolean,
        guard: Boolean,
        jazz: Boolean,
        perc: Boolean,
      },
      stripeTknId: String,
      amount: Number,
      inserted: {type: Date, default: Date.now}
    });

var completeDonationModel   = mongoose.model("CompleteDonation", completeDonation);
var donationWOPaymentModel  = mongoose.model("donation-WO-Payment", donationWOPayment);

// The Email library in use 
var emailObj = require('./lib/emailFunctions');