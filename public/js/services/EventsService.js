angular.module('SRHSMusicApp')
.factory('EventsService', ['$http', function ($http) {
    var eventsData = {
        submitDonation: function (donation, stripeToken) {
        	var reg = { 'donationDetails': donation, 'stripeToken': stripeToken};
        	return $http.post("/donate", reg);
        }
    }
    return eventsData;
}]);