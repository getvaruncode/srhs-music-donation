// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('SRHSMusicApp', ['ui.router', 'stripe.checkout'])

// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
    
        // route to show our basic form (/form)
        .state('donation', {
            url: '/donation',
            templateUrl: 'donationForm.html',
            abstract: true,
            controller: 'formController'
        })  
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/profile)
        .state('donation.info', {
            url: '/info',
            templateUrl: 'donation-info.html'
        })
        // url will be /form/payment
        .state('donation.confirm', {
            url: '/confirm',
            templateUrl: 'donation-confirm.html'
        })  
        .state('donation.thankyou', {
            url: '/finish',
            templateUrl: 'thanks.html'
        })      
 
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/donation/info');
})

// our controller for the form
// =============================================================================
.controller('formController', ['$scope', '$state', '$rootScope', 'EventsService', '$window', function($scope, $state, $rootScope, EventsService, $window) {
    $scope.formData = {};
    $scope.formData.applyDonation = "General Music Fund";
    $scope.formData.subscribe = {general: true};

    $scope.debug = false;
    if ($scope.debug) {
        $scope.formData.donor = {
            name: "Varun Bhajekar",
            email: "varun.bhajekar@gmail.com",
            phone: "929-929-9299"
        };
        $scope.formData.amount = 200;
        $scope.formData.employerMatch = true;
        $scope.formData.employer = "Oracle";
    }

    $scope.processing = false;

    $scope.disableConfirm = function() {
        var isDisabled = true;
        var formData = $scope.formData;
        if (formData) {
            if (formData.donor && formData.amount) {
                if (formData.donor.name && formData.donor.email) {
                    if (formData.employerMatch) {
                        if (formData.employer) {
                            isDisabled = checkEmail(formData.subscribe);
                        }
                    } else {
                        isDisabled = checkEmail(formData.subscribe);
                    }
                }
            }
        }
        return isDisabled;
    };

    function checkEmail(emailObject) {
        var isDisabled = false;
        if (emailObject) {
            if (emailObject.emailUpdates) {
                isDisabled = true;
                if (emailObject.general || emailObject.boosters || emailObject.band || emailObject.orch || emailObject.march || emailObject.guard || emailObject.jazz || emailObject.perc) {
                    isDisabled = false;
                }
            }
        }
        return isDisabled;
    }

    $scope.list = null;

    $scope.enableEmailUpdate = function() {
        $scope.list = "";
        var isEnabled = false;
        if ($scope.formData) {
            if ($scope.formData.subscribe) {
                var subscribe = $scope.formData.subscribe;
                if (subscribe.emailUpdates) {
                    if (subscribe.general) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "General SRHS Music";
                    }
                    if (subscribe.boosters) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Music Boosters";
                    }
                    if (subscribe.band) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Symphonic Band";
                    }
                    if (subscribe.orch) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Orchestra";
                    }
                    if (subscribe.march) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Marching Band";
                    }
                    if (subscribe.guard) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Color Guard";
                    }
                    if (subscribe.jazz) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Jazz Band";
                    }
                    if (subscribe.perc) {
                        isEnabled = true;
                        if ($scope.list) $scope.list += ", ";
                        $scope.list += "Percussion Ensemble";
                    }
                }
            }
        }
        return isEnabled;
    }
    
    $scope.doCheckout = function(token) {
            $scope.processing = true;
            $scope.confirm = {};
            $scope.confirm.name = $scope.formData.donor.name;
            $scope.confirm.email = $scope.formData.donor.email;
            $scope.confirm.amount = $scope.formData.amount;
            //$scope.formData = {};

            EventsService.submitDonation($scope.formData, token)
            .success(function(data) {
                var returnObj = angular.toJson(data, true);
                var charge = JSON.parse(returnObj);      
                    if (!charge) {
                        $scope.errorMsg = "Card not accepted: Please try again";
                        $scope.processing = false;
                        $state.go("donation.confirm");
                    } else {
                        if (charge.errorMsg) {
                            $scope.errorMsg = charge.errorMsg;
                            $scope.processing = false;
                            $state.go("donation.confirm");
                        } else {
                            if (charge.captured) {
                                $scope.regID = charge.regID;
                                $scope.formData = {};
                                $state.go("donation.thankyou");
                            } else {
                                $scope.errorMsg = "Card Denied: Please try again";
                                $scope.processing = false;
                                $state.go("donation.confirm");
                            }
                        }
                    }
                })
                .error(function(err) {
                    $scope.processing = false;
                    if (err) console.log('Error: ' + err.toString());
                    $state.go("form.confirm");
                });
    };

    $scope.goTo = function() {
        $window.location.href = $scope.destination;
    }

}]);