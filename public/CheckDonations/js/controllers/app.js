// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('SRHSMusicApp', ['ui.router', 'ngSanitize', 'ngCsv', 
                                'auth0', 'angular-storage', 'angular-jwt'])

// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider, $httpProvider, authProvider, jwtInterceptorProvider) {
    $stateProvider
        .state('base', {
            url: '/base',
            abstract: true,
            templateUrl: 'templates/include.html',
            controller: 'formController',
            data: { 
                requiresLogin: true 
            }
        })
        .state('base.donation', {
            url: '/donation',
            templateUrl: 'templates/getDonation.html'
        })
        .state('base.display', {
            url: '/display',
            templateUrl: 'templates/display.html'
        })
        .state('login', { 
            url: '/login', 
            templateUrl: 'templates/loginLaunch.html', 
            controller: 'LoginCtrl'
        })
        ;
    authProvider
        .init({
            domain: AUTH0.domain,
            clientID: AUTH0.client,
            loginState: 'login'
        });
    authProvider
        .on('loginSuccess', function($location, profilePromise, idToken, store) {
            console.log("Login Success");
            profilePromise.then(function(profile) {
                store.set('profile', profile);
                store.set('token', idToken);
                $location.path('/base/donation');
            });
        });
    authProvider
        .on('loginFailure', function() {
            alert("Error");
        });
    authProvider
        .on('authenticated', function($location) {
            console.log("Authenticated");
        });

    jwtInterceptorProvider.tokenGetter = ['store', function(store) {
        var token = store.get('token');
        return token;
    }];

    $httpProvider.interceptors.push('jwtInterceptor');
    // send users to the form page 
    $urlRouterProvider.otherwise('/base/donation');
})
.run(function($rootScope, auth, store, jwtHelper, $location, $window) {
  $rootScope.$on('$locationChangeStart', function() {
    var search = $location.$$path.substring(1)
        .split(/[&||?]/)
        .filter(function (x) { return x.indexOf("=") > -1; })
        .map(function (x) { return x.split(/=/); })
        .map(function (x) {
            x[1] = x[1].replace(/\+/g, " ");
            return x;
        })
        .reduce(function (acc, current) {
            acc[current[0]] = current[1];
            return acc;
        }, {});

    if (Object.keys(search).length) {
        console.log("Search Parameters are " + JSON.stringify(search));
        if (search.error && search.error_description) {
            $rootScope.errorMessage = search.error_description;
        }
    }
    var token = store.get('token');
    if (token) {
        console.log("Token is " + token);
        if (jwtHelper.isTokenExpired(token)) {
            console.log("Token has expired");
            store.remove('token');
            store.remove('profile');
            $location.path('/login');
        } else {
            if (auth.isAuthenticated) {
                console.log("We have a good token!");
            } else {
                if (!store.get('profile')) {
                    $location.path('/login');
                } else {
                    auth.authenticate(store.get('profile'), token);
                }
            }
        }
    } else {
        console.log("No token found!");
        store.remove('profile');
        $location.path('login');
    }
  });
})

// our controller for the form
// =============================================================================
.controller('formController', ['$scope', '$state', '$rootScope', 'DonationService', '$window', 'auth', 'store',
    function($scope, $state, $rootScope, DonationService, $window, auth, store) {
    $scope.formData = {};
    $scope.formData.startDate = '2014-11-26';
    $scope.formData.endDate = '2017-11-26';
    $rootScope.errorMessage = ''; 
    $scope.profile = store.get('profile');

    $scope.findDonations = function () {
        $rootScope.errorMessage = ''; 
        $scope.startDate = new Date ($scope.formData.startDate + "T00:00:00+04:00");
        $scope.endDate = new Date ($scope.formData.endDate + "T00:00:00+04:00");
        DonationService.getDonations($scope.startDate, $scope.endDate)
            .success(function(data) {
                $scope.results = [];
                $scope.donations = data;
                $scope.donations.forEach (function (element, index, array) {
                    element.payment.amount = (element.payment.amount/100).toFixed(2);
                    if (element.inserted) {
                        var d = new Date(element.inserted);
                        element.inserted = d.toLocaleString();
                    }
                    var newElement = {"name": element.name,
                                      "email": element.email,
                                      "phone": element.phone,
                                      "amount": "$" + element.payment.amount,
                                      "employer": element.employer,
                                      "donationGoesTo": element.donationGoesTo,
                                      "subscriptions": $scope.getDonationString(element),
                                      "date": element.inserted
                                     };
                    $scope.results.push(newElement);
                });
                $state.go("base.display");
            })
            .error(function(err) {
                console.log(JSON.stringify(err));
                if (err.message) 
                    $rootScope.errorMessage = "Unknown Error(" + err.message + ": Login Again";
                if (err.name && err.name === 'UnauthorizedError') {
                    console.error('Invalid credentials');
                    $rootScope.errorMessage = "Its been a while since you have signed in. Continue by authenticating yourself again!";
                }
                $scope.logOut();
            });
    }

    $scope.logOut = function () {
        auth.signout();
        store.remove('token');
        store.remove('profile');
        $state.go("login");
    }

    $scope.goBack = function () { 
        $state.go('base.donation');
    }

    $scope.getDonationString = function (donation) {

        var donationString = "";

        if(donation.subscribe && donation.subscribe.general) {
            donationString += "General, ";
        };
        if(donation.subscribe && donation.subscribe.boosters) {
        donationString +=  "Music Boosters, ";
        };
        if(donation.subscribe && donation.subscribe.band) {
        donationString +=  "Band, ";
        };
        if(donation.subscribe && donation.subscribe.orch) {
        donationString +=  "Orchestra, ";
        };
        if(donation.subscribe && donation.subscribe.march) {
        donationString +=  "Marching Band, ";
        };
        if(donation.subscribe && donation.subscribe.guard) {
        donationString +=  "Color Guard, ";
        };
        if(donation.subscribe && donation.subscribe.jazz) {
        donationString +=  "Jazz Band, ";
        };
        if(donation.subscribe && donation.subscribe.perc) {
        donationString +=  "Percussion, ";
        };
        return donationString;
    };

    $scope.filename = "test";
    $scope.getHeader = function () {return ["Name", "Email", "Phone", "Amount", "Employer", "Donation Category", "Subscription(s)", "Date of Donation"]};

    $scope.getSecuredPing = function () {
        DonationService.securedPing().success(
            function successCallback(response) {
                console.log(response);
            })
            .error( function errorCallback(error) {
                console.log(error);
            });
    }
}]);