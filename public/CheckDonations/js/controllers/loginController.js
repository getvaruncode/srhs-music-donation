angular.module('SRHSMusicApp')
.controller('LoginCtrl', function ($scope, $rootScope, auth) {
	$scope.auth = auth;
	$scope.errorMessage = $rootScope.errorMessage;

	var lock_options = {
		'icon': 'https://srhs-music-donation.herokuapp.com/css/images/stripeLogo.gif',
		'connections': ['google-oauth2', 'facebook'],
		//'container': 'login-widget',
		'dict' : {'signin' : {'title': 'SRHS Music Admin'}}
	};

	$scope.login = function () {
		auth.signin(lock_options, function () {}, function () {});
	}
});
