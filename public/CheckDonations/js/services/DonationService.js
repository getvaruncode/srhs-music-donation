angular.module('SRHSMusicApp')
    .factory('DonationService', ['$http', function ($http) {
    var donationFunctions = {
        sayHello: function () {
            return $http.get("/helloworld");
        },
        getDonations: function (startDate, endDate) {
        	var reg = { 'startDate': startDate, 'endDate': endDate};
        	return $http.post("/secured/getDonations", reg);
        },
        securedPing: function () {
            return $http.get("/secured/ping");
        }
    }
    return donationFunctions;
}]);