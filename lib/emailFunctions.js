var emailKey = process.env.EMAIL_KEY || "Enter sendgrid email key";
var sendgrid = require("sendgrid")(emailKey);
var handlebars = require("handlebars");

var fs = require('fs');
var contents = fs.readFileSync('templates/email.html').toString();
var template = handlebars.compile(contents);

function sendMail(donation, callback) {
	var email = new sendgrid.Email();
	var data = {
		donor: {
			name: donation.donor.name,
			email: donation.donor.email
		},
		payment: {
			amount: donation.amount
		},
		regID: donation.regID
	};
	console.log(donation);
	email.addTo(donation.donor.email);
	email.setFrom("dontteply@sendgrid.net");
	email.setFromName("SRHS Music Boosters");
	email.setSubject("Thank you for your donation");
	var outputHTML = template(data);
	if (outputHTML) {
		email.setHtml(outputHTML);
	} else {
		email.setHtml("Your SRHS Music Boosters donation has succeeded");
	}

	sendgrid.send(email, function (err, json) {
  		if (err) { 
  			callback(err); 
  		} else {
  			callback(null, json);
  		}});
}

module.exports.sendMail = sendMail;