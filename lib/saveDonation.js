
function writewithPayment(requestInfo, regModel, charge, authID, callWhenDone) {

  var newRecord = initializeRecord(requestInfo, regModel);
  newRecord.payment = {
                        "tokenId"     : requestInfo.stripeToken.id,
                        "authkey"     : authID,
                        "chargeId"    : charge.id,
                        "amount"      : charge.amount,
                        "cardLastFour": charge.source.last4,
                        "brand"       : charge.source.brand
                      }

  newRecord.save(function (err, insertedRecord) {
    if (err) {
      callWhenDone("Insert Failed " + err);
    } else {
      //console.log("Inserted Registration " + insertedRecord);
      callWhenDone(null, insertedRecord);
    } // end of if-else
  }); // end of requestInfo.save()
} // end of insertIntoDB()


function writeWOPayment(requestInfo, regModel, callWhenDone) {
  var newRecord = initializeRecord(requestInfo, regModel);
  newRecord.stripeTknId = requestInfo.stripeToken.id;
  newRecord.amount = requestInfo.donationDetails.amount*100;

  newRecord.save(function (err, insertedRecord) {
    if (err) {
      callWhenDone("Insert Failed " + err);
    } else {
      console.log("Inserted Auth " + insertedRecord);
      callWhenDone(null, insertedRecord);
    } // end of if-else
  }); // end of requestInfo.save()
} // end of insertIntoDB()


function initializeRecord (requestInfo, regModel) {
  var donation = requestInfo.donationDetails;
  var stripeTkn = requestInfo.stripeToken;

  var newRecord = new regModel({
      name: donation.donor.name,
      email: donation.donor.email,
      phone: donation.donor.phone,
      donationGoesTo: donation.applyDonation,
  });

  if (donation.subscribe != null) {
    var emailList = donation.subscribe;
    newRecord.subscribe = {};
    if (emailList.general)    newRecord.subscribe.general   = emailList.general;
    if (emailList.boosters)   newRecord.subscribe.boosters  = emailList.boosters;
    if (emailList.band)       newRecord.subscribe.band      = emailList.band;
    if (emailList.orch)       newRecord.subscribe.orch      = emailList.orch;
    if (emailList.march)      newRecord.subscribe.march     = emailList.march;
    if (emailList.guard)      newRecord.subscribe.guard     = emailList.guard;
    if (emailList.jazz)       newRecord.subscribe.jazz      = emailList.jazz;
    if (emailList.perc)       newRecord.subscribe.perc      = emailList.perc;
  } /*else {
    newRecord.subscribe = false;
  }*/

  if (donation.employerMatch != null) {
    newRecord.employerMatch = true;
    newRecord.employer = donation.employer;
  } else {
    newRecord.employerMatch = false;
  }
  return newRecord;
}

module.exports.saveWithPayment = writewithPayment;
module.exports.saveWOPayment   = writeWOPayment;