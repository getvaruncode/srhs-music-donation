var	MONGO_HOST = process.env.MONGO_HOST || 'localhost';
var	MONGO_PORT = process.env.MONGO_PORT || 27017;
var	MONGO_DB   = process.env.MONGO_DB || 'ChessClients';
var MONGO_USER = process.env.MONGO_USER || 'chessadmin';
var MONGO_PWD  = process.env.MONGO_PWD || 'password';

var MONGO_URL = 'mongodb://' + MONGO_HOST + ':' + MONGO_PORT + "/" + MONGO_DB;

module.exports.MONGO_URL  = MONGO_URL;
module.exports.MONGO_USER = MONGO_USER;
module.exports.MONGO_PWD  = MONGO_PWD;