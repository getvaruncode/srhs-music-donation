
function getDonations(model, startDate, endDate, callWhenDone) {

  var queryOptions = {"$and": [
                                { inserted: {"$gte": startDate}}, 
                                { inserted: {"$lte": endDate}}
                              ]
                     };

  model.find( queryOptions, function(err, result) {
    if (err) {
      callWhenDone(err);
    } else {
      //console.log(JSON.stringify(result));
      //console.log("Found " + result.length + " donations.")
      callWhenDone(null, result);
    }
  });
}

module.exports.getDonations = getDonations;