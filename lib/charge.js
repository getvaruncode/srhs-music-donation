// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
// var stripe = require("stripe")("sk_test_BQokikJOvBiI2HlWgH4olfQ2");

var stripeKey = process.env.STRIPE_KEY || "sk_test_smCNlC4kBYx5XinkRcnuxv7y";

var stripe = require("stripe")(stripeKey);

function chargeCC(paymentAmt, email, tokenID, callback) {	// Amount is in cents, tokenID is gathered from initial authorization

  stripe.charges.create({
  		amount: paymentAmt,
  		currency: "usd",
  		source: tokenID,
  		description: "SRHS Music Donations",
      receipt_email: email
	}, function(err, charge) {
  		if (err) {
        var obj = JSON.parse(JSON.stringify(err, null, 4));
        if (err.type === 'StripeCardError') {
          obj.errorMsg = "Credit Card is declined";
        } else {
          obj.errorMsg = "Credit Card: Error";
        }
        callback(obj);
  		} else {
        var obj = JSON.parse(JSON.stringify(charge, null, 4));
        if (obj.captured) {
          callback(null, obj);
        } else {
          obj.errorMsg = "Credit Card: Capture Error";
          callback(obj);
        }
  		}
	});
}

module.exports.charge = chargeCC;